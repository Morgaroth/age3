/*
 * Copyright (C) 2014 Intelligent Information Systems Group.
 *
 * This file is part of AgE.
 *
 * AgE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AgE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AgE.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Created: 2014-10-16.
 */

package org.age.console.command;

import ch.qos.logback.classic.spi.ILoggingEvent;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.google.common.base.Strings;
import com.hazelcast.core.Message;
import org.age.services.logs.LogsService;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.MoreObjects.toStringHelper;

/**
 * Command for getting info about and managing the whole cluster.
 */
@Named
@Scope("prototype")
@Parameters(commandNames = "logs", commandDescription = "Logs management. Date format: yyyy-MM-dd_hh:mm:ss.", optionPrefixes = "--")
public final class LogsCommand extends BaseCommand {

    private enum Operation {
        LIST("list"),
        SAVE("save");

        private final String operationName;

        Operation(final @NonNull String operationName) {
            this.operationName = operationName;
        }

        public String operationName() {
            return operationName;
        }
    }

    private static final Logger log = LoggerFactory.getLogger(LogsCommand.class);
//
//    @Inject
//    private @NonNull HazelcastInstance hazelcastInstance;

    @Inject
    private @NonNull
    @Named("topic")
    LogsService service;

    @Parameter(names = "--dateFrom")
    private String dateFrom;

    @Parameter(names = "--dateTo")
    private String dateTo;

    @Parameter(names = "--level")
    private String level;

    @Parameter(names = "--task")
    private String task;

    @Parameter(names = "--nodeId")
    private String nodeId;

    @Parameter(names = "--outputFile")
    private String outputFile;

    public LogsCommand() {
        addHandler(Operation.LIST.operationName(), this::listLogs);
        addHandler(Operation.SAVE.operationName(), this::saveLogsToFile);
    }

    /**
     * Prints information about logs
     */
    private void listLogs(final @NonNull PrintWriter printWriter) {
        List<Message<ILoggingEvent>> logs = service.getAllLogs();
        log.error("Listing  {} logs", logs.size());
        Optional<PrintWriter> optFileOutput = Optional.empty();
        if (!Strings.isNullOrEmpty(dateFrom)) {
            logs = filterByDateFrom(logs);
        }
        if (!Strings.isNullOrEmpty(dateTo)) {
            logs = filterByDateTo(logs);
        }
        if (!Strings.isNullOrEmpty(level)) {
            logs = filterByLevel(logs);
        }
        if (!Strings.isNullOrEmpty(nodeId)) {
            logs = filterByNode(logs);
        }
        if (!Strings.isNullOrEmpty(task)) {
            logs = filterByTask(logs);
        }
        if (!Strings.isNullOrEmpty(outputFile)) {
            File file = new File(outputFile);
            try {
                if ((file.exists() && file.canWrite()) || (!file.exists() && file.createNewFile())) {
                    optFileOutput = Optional.of(new PrintWriter(new FileOutputStream(file)));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (logs != null) {
            final Optional<PrintWriter> finalOptFileOutput = optFileOutput;
            logs.forEach(entry -> printLog(entry, printWriter, finalOptFileOutput));
        }
    }

    /**
     * Filters logs by dateFrom parameter
     */
    private List<Message<ILoggingEvent>> filterByDateFrom(List<Message<ILoggingEvent>> logs) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss");
            Date parsedDate = dateFormat.parse(dateFrom);
            long timestamp = parsedDate.getTime();
            log.debug("logs size before filtering: " + logs.size() + " filtering by timestamp: " + timestamp);
            List<Message<ILoggingEvent>> logsAfterFilter = logs.stream().filter(entry -> entry.getMessageObject().getTimeStamp() > timestamp).collect(Collectors.toList());
            return logsAfterFilter;
        } catch (Exception e) {
            log.error(e.getStackTrace().toString());
            return null;
        }
    }

    /**
     * Filters logs by dateTo parameter
     */
    private List<Message<ILoggingEvent>> filterByDateTo(List<Message<ILoggingEvent>> logs) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss");
            Date parsedDate = dateFormat.parse(dateTo);
            long timestamp = parsedDate.getTime();
            log.debug("logs size before filtering: " + logs.size() + " filtering by timestamp: " + timestamp);
            List<Message<ILoggingEvent>> logsAfterFilter = logs.stream().filter(entry -> entry.getMessageObject().getTimeStamp() < timestamp).collect(Collectors.toList());
            return logsAfterFilter;
        } catch (Exception e) {
            log.error(e.getStackTrace().toString());
            return null;
        }
    }

    /**
     * Filters logs by level parameter
     */
    private List<Message<ILoggingEvent>> filterByLevel(List<Message<ILoggingEvent>> logs) {
        List<Message<ILoggingEvent>> logsAfterFilter = logs.stream().filter(entry -> entry.getMessageObject().getLevel().levelStr.equalsIgnoreCase(level)).collect(Collectors.toList());
        return logsAfterFilter;
    }

    /**
     * Filters logs by node parameter
     */
    private List<Message<ILoggingEvent>> filterByNode(List<Message<ILoggingEvent>> logs) {
        List<Message<ILoggingEvent>> logsAfterFilter = logs.stream().filter(entry -> entry.getPublishingMember().getUuid().equalsIgnoreCase(nodeId)).collect(Collectors.toList());
        return logsAfterFilter;
    }

    /**
     * Filters logs by node parameterc
     */
    private List<Message<ILoggingEvent>> filterByTask(List<Message<ILoggingEvent>> logs) {
        List<Message<ILoggingEvent>> logsAfterFilter = logs.stream().filter(entry -> entry.getMessageObject().getThreadName().equalsIgnoreCase(task)).collect(Collectors.toList());
        return logsAfterFilter;
    }

    /**
     * Prints logs to file.
     */
    private void saveLogsToFile(final PrintWriter printWriter) {
        log.debug("Printing logs to file.");
    }

    private void printLog(final Message<ILoggingEvent> entry, final @NonNull PrintWriter printWriter, final @NonNull Optional<PrintWriter> optFileOuput) {
        ILoggingEvent plog = entry.getMessageObject();
        String logBody = String.format("%s - %s", plog.getLevel().levelStr, plog.getMessage());
        printWriter.println(logBody);
        optFileOuput.ifPresent(fileOutput -> fileOutput.println(logBody));
    }

    @Override
    public String toString() {
        return toStringHelper(this).toString();
    }
}