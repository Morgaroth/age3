package org.age.services.logs.internal;

import ch.qos.logback.classic.spi.ILoggingEvent;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;
import org.age.services.logs.LogsConfiguration;
import org.age.services.logs.LogsService;
import org.checkerframework.checker.nullness.qual.MonotonicNonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.LinkedList;
import java.util.List;

@Named("topic")
public class TopicLogsService implements LogsService, MessageListener<ILoggingEvent> {
    private static final Logger log = LoggerFactory.getLogger(TopicLogsService.class);

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    private String topicName = LogsConfiguration.TOPIC_NAME;

    @Inject
    private @MonotonicNonNull HazelcastInstance hazelcastInstance;

    private @MonotonicNonNull ITopic<ILoggingEvent> topic;

    @PostConstruct
    private void construct() {
        topic = hazelcastInstance.getTopic(topicName);
        topic.addMessageListener(this);
    }

    private List<Message<ILoggingEvent>> logs = new LinkedList<>();

    protected TopicLogsService addLog(Message<ILoggingEvent> entry) {
        logs.add(entry);
        return this;
    }

    @Override
    public List<Message<ILoggingEvent>> getAllLogs() {
        return logs;
    }

    @Override
    public void onMessage(Message<ILoggingEvent> message) {
        /*log.debug(
                "Log received {} from {}",
                message.getMessageObject(),
                message.getPublishingMember().getUuid()
        );*/
        addLog(message);
    }
}
