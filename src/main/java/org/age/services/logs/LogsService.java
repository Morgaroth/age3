package org.age.services.logs;

import ch.qos.logback.classic.spi.ILoggingEvent;
import com.hazelcast.core.Message;

import java.util.List;

public interface LogsService {
    List<Message<ILoggingEvent>> getAllLogs();
}
