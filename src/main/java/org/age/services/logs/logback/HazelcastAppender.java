package org.age.services.logs.logback;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEventVO;
import ch.qos.logback.core.AppenderBase;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ITopic;
import org.age.services.logs.LogsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hazelcast appender, it should be able to connect to specified hazelcast grid, then
 * connect to defined queue, and send logs to console node by this queue
 */
public class HazelcastAppender extends AppenderBase<ILoggingEvent> {

    private static final Logger log = LoggerFactory.getLogger(HazelcastAppender.class);

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    private String topicName = LogsConfiguration.TOPIC_NAME;
    private ITopic<ILoggingEvent> topic;

    @Override
    protected void append(ILoggingEvent eventObject) {
        try {
            topic.publish(LoggingEventVO.build(eventObject));
        } catch (Throwable e) {
            System.err.println("error encountered " + e.getMessage());
        }
    }

    @Override
    public void start() {
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
        topic = hz.getTopic(topicName);
        System.err.println("hazelcast logger constructed");
        started = true;
    }
}
